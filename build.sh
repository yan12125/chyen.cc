set -o errexit

shopt -s extglob

pacman -Syu --noconfirm
pacman -S --noconfirm --needed git pelican make python-markdown curl

sed -i '/NoExtract.*usr\/share\/i18n/d' /etc/pacman.conf
pacman -S --noconfirm glibc
echo zh_TW.UTF-8 UTF-8 >> /etc/locale.gen
locale-gen -a

git submodule update --init --recursive

mkdir public
cp -rv !(blog|public|.*) -t public
make -C blog OUTPUTDIR="$PWD/public/blog"
