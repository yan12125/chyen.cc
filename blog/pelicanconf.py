#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import markdown  # ensure that markdown files will be processed
assert markdown  # to make pyflakes happy

AUTHOR = 'Chih-Hsuan Yen'
SITENAME = '似水年華'
SITEURL = 'https://chyen.cc/blog'

PATH = 'content'

TIMEZONE = 'Asia/Taipei'

# Not sure how it works..
DEFAULT_LANG = 'zh_tw'
DATE_FORMATS = {
    # Pelican uses Python datetime format codes. See:
    # https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes
    # Note that the locale name is os-specific. Here I assume UNIX-like systems.
    'zh_tw': ('zh_TW.UTF-8', '%Y/%m/%d (%A)'),
}

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = False

ARTICLE_URL = ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/{date:%d}/{slug}.html'
PAGE_URL = PAGE_URL_SAVE_AS = 'pages/{slug}.html'

PLUGIN_PATHS = ['./render_math/pelican/plugins']
PLUGINS = ['render_math']

STATIC_PATHS = ['files', 'images']
