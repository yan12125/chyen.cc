Title: 孤單北半球
Date: 2013-07-30 19:12
Author: Chih-Hsuan Yen
Category: Something fun
Slug: 孤單北半球
Status: published

<style type="text/css">
/* for vimdiff below */
table { color: #d0d0d0; background-color: #3a3a3a; }
.DiffChange { background-color: #3a3a3a; padding: 0px; margin: 0px; }
.DiffText { background-color: #5f005f; }
table { table-layout: fixed; }
table, tbody { width: 100%; margin: 0; padding: 0; }
td { width: 50.0%; white-space: nowrap; }
</style>
有一天在和幾個同學唸書，有人忽然提到孤單北半球這首歌。這首歌最早是誰唱的？眾說紛紜，不一而足。或曰林依晨，或曰歐得洋。上Google一查，原唱是歐得洋。長久以來，我和幾位同學一直以為是林依晨原唱，竟是個天大的笑話！先入為主的成見和憑空臆測使我們長久以來抱持錯誤的認知，若非網路上訊息的快速流通，孤單北半球這首歌的原唱，只怕是和我們的心靈無緣了。胡適嘗曰：『大膽假設，小心求證』，但在人來人往，駢肩雜沓的年代，連求證也難了，實不可不慎！

歐得洋和林依晨的版本，兩者又是另一個故事。就音色言，各有別致韻味，此乃見仁見智，暫且不表。且說兩者歌詞。以下歐得洋版的歌詞（取自[魔鏡歌詞網](http://mojim.com/)）：

<pre>
<div style="color: #446622">
用我的晚安陪你　吃早餐　記得把想念　存進撲滿
我　望著滿天星　在閃　聽牛郎對織女說　要勇敢

別怕我們在地球　的兩端　看我的問候　騎著魔毯
飛　用光速飛到你面前　要你能看到十字星有北極星作伴

少了我的手臂當枕頭　你習不習慣
你的望遠鏡望不到　我北半球的孤單
太平洋的潮水跟著地球　來迴旋轉
我會耐心地等　隨時歡迎你靠岸

少了我的懷抱當暖爐　你習不習慣
E給你照片看不到　我北半球的孤單
世界再大兩顆真心就能　互相取暖
想念不會偷懶　我的夢通通給你保管
</div>
</pre>

而林依晨版的歌詞則是：

<pre>
<div style="color: #446622">
用你的早安陪我　吃晚餐　記得把想念　存進撲滿
我　望著滿天星　在閃　聽牛郎對織女說　要勇敢

不怕我們在地球　的兩端　看你的問候　騎著魔毯
飛　用光速飛到我面前　你讓我看到北極星有十字星作伴

少了你的手臂當枕頭　我還不習慣
你的望遠鏡望不到　我北半球的孤單
太平洋的潮水跟著地球　來迴旋轉
我會耐心地等　等你有一天靠岸

少了你的懷抱當暖爐　我還不習慣
E給你照片看不到　我北半球的孤單
世界再大兩顆真心就能　互相取暖
想念不會偷懶　我的夢通通給你保管
</div>
</pre>

[vimdiff](http://stackoverflow.com/questions/7475244/save-vimdiff-output)一下：

<table width="100%" border="1">
<tbody>
<tr>
<td>孤單北半球-林依晨.txt</td>
<td>孤單北半球-歐得洋.txt</td>
</tr>
<tr>
<td valign="top">
<pre class="DiffChange">
<div>用<span class="DiffText">你的早安陪我　吃晚</span>餐　記得把想念　存進撲滿 
我　望著滿天星　在閃　聽牛郎對織女說　要勇敢

<span class="DiffText">不怕我們在地球　的兩端　看你</span>的問候　騎著魔毯
飛　用光速飛到<span class="DiffText">我面前　你讓我看到北極星有十字</span>星作伴

少了<span class="DiffText">你的手臂當枕頭　我還</span>不習慣
你的望遠鏡望不到　我北半球的孤單
太平洋的潮水跟著地球　來迴旋轉
我會耐心地等　<span class="DiffText">等你有一天</span>靠岸

少了<span class="DiffText">你的懷抱當暖爐　我還</span>不習慣
E給你照片看不到　我北半球的孤單
世界再大兩顆真心就能　互相取暖
想念不會偷懶　我的夢通通給你保管

</div>
</pre>
</td>
<td valign="top">
<pre class="DiffChange">
<div>用<span class="DiffText">我的晚安陪你　吃早</span>餐　記得把想念　存進撲滿
我　望著滿天星　在閃　聽牛郎對織女說　要勇敢

<span class="DiffText">別怕我們在地球　的兩端　看我</span>的問候　騎著魔毯
飛　用光速飛到<span class="DiffText">你面前　要你能看到十字星有北極</span>星作伴

少了<span class="DiffText">我的手臂當枕頭　你習</span>不習慣
你的望遠鏡望不到　我北半球的孤單
太平洋的潮水跟著地球　來迴旋轉
我會耐心地等　<span class="DiffText">隨時歡迎你</span>靠岸

少了<span class="DiffText">我的懷抱當暖爐　你習</span>不習慣
E給你照片看不到　我北半球的孤單
世界再大兩顆真心就能　互相取暖
想念不會偷懶　我的夢通通給你保管

</div>
</pre>
</td>
</tr>
</tbody>
</table>

大多相同，而不同之處畫龍點睛，有如兩人對話。編詞的用心，可見一斑。
