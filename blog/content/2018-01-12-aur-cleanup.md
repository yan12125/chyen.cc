Title: AUR cleanup
Date: 2018-01-12 21:51
Authors: Chih-Hsuan Yen
Slug: aur-cleanup

今天把[yan12125/aur](https://github.com/yan12125/aur)裡已經disowned的package都刪掉了。

[yan12125/aur@47252d3d8020c1011068587355270d9ae62b9db5](https://github.com/yan12125/aur/commit/47252d3d8020c1011068587355270d9ae62b9db5)

有種斷捨離的感覺...

有些曾經是我追尋的目標，有則完全想不起來當初是用來做什麼的。無論如何，過去
的，總是該過去了。

一些印象：

* boringssl-aur：在搞aur自動上傳的時候加的。[pygit2](https://github.com/libgit2/pygit2/issues/552) -> libgit -> [libssh2](https://github.com/libssh2/libssh2/issues/39#issuecomment-285889417)
* firefox-adblock-plus-hg：當初好像adblock plus有一些bug，只有在hg上的版本是正常運作的。現在我跳槽到ublock origin，而Firefox預設也不能安裝沒有數位簽章的外掛了。
* flashplugin-beta：看到Phoronix上[Adobe Flash要重新支援Linux的新聞](https://www.phoronix.com/scan.php?page=news_item&px=Adobe-Will-Update-Linux-Flash)。有些網站在舊的Flash (11.x)上不執行，就去打包這個。在這之前，用[PPAPI轉NPAPI的外掛](https://github.com/i-rinat/freshplayerplugin)用了一段時間，常常會有不穩定的情況。
* gcc46-multilib：印象中是舊版的是AOSP或Firefox OS用到的。
* js45：很多project (e.g. [libproxy](https://github.com/libproxy/libproxy/issues/32))用了舊版的spidermonkey，我試著porting了一些，都沒有upstream。中間送了不少build system patch給mozilla。後來gjs用到了js45，他就進官方repo了。
* k-map：大二修交換電路的時候打包的，算是我最早的幾個package之一。原來我用了Arch之後不久就開始打包package了。用Ubuntu一年多都沒打包過，Arch真的會改變一個人的思維。
* lxqt-panel-git：在還沒成為lxqt member時，曾短暫維護這個package一陣子。
* megatools-dev-git：本以為會是下一版megatools，想不到舊的branch更新還比較多。
* mips-mentor-linux-gnu：似乎是計算機結構的final project[跑一個simulator](https://github.com/libproxy/libproxy/issues/32)用的。
* purple-line-git：本來希望可以在Linux上用LINE，結果把LINE帳號搞的亂七八糟的。也不支援貼圖。
* pyalpm-git：pyalpm有些bug的時候維護的，似乎是和新版Python不相容
* pypy3-hg：在pypy3還沒到3.5-compatible的時候用的
* python-pysvn：一個難搞的package，C++有太多陷阱
* tamarin-redux-hg：以前用來研究ActionScript的，後來Flash慢慢消失了
* xx-net：曾經想要做成package，就不用常常從git抓，然而XX-Net的設計理念不適合做成package

這裡面js45 (libproxy), pypy3-hg, tamarin-redux-hg, xx-net多少和youtube-dl有關係。想想從youtube-dl出來的side project還真不少。後來有些變成單獨的project了，例如python3-android。研究VPN, proxy等等的過程中，也了解的不少中國網路的困境，進而對中國政治產生一些興趣。

順帶一提，youtube-dl一開始是為了讓女友可以[載streetvoice上的音樂](https://github.com/rg3/youtube-dl/pull/4758)才認真去研究的。說來可以算是從小打小鬧進入嚴肅的open source世界的濫觴。有空再來寫單獨一篇XD
