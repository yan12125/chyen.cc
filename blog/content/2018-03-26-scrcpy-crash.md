Title: 解決scrcpy crash的問題
Date: 2018-03-27 20:10
Authors: Chih-Hsuan Yen
Slug: scrcpy-crash

之前聽說scrcpy可以把Android的螢幕同步到電腦上。第一次裝沒跑成功，crash了。

經過一番測試，發現似乎SDL(scrcpy用的GUI library)會嘗試載入ibus，而強制指定`export SDL_IM_MODULE=fcitx`後可以正常執行scrcpy。正當準備提bug時，Arch Linux的sdl2來了一份更新，此後不需要`SDL_IM_MODULE`也不會crash了。

不知是否相關？聊表紀錄。
