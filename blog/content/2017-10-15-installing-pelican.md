Title: 安裝 Pelican
Date: 2017-10-15 01:31
Authors: Chih-Hsuan Yen
Slug: installing-pelican

來玩玩自己的部落格～把pelican裝起來，寫篇測試文章

TODO:

1. Deploy - 用git hook似乎是個選項
2. Themes - http://www.pelicanthemes.com/上面滿多選擇的
3. Git設定 - ``.gitignore``, 放到Github等等
4. 設定文章網址，讓他裡面有日期
5. 字型 - 標題預設的中文字型頗怪

可能會做的事：

1. 留言系統 - farseerfc分享了一個相當有趣的點子：[使用Github issues當作留言系統](https://farseerfc.me/github-issues-as-comments.html)。不過我還是別用那麼geek的系統好了。

UPDATE 2017/10/23:

1. Deploy懶得研究那麼多了XD 直接在server上clone一份，Apache的設定檔指過去，搞定收工！
2. 先放到我自己的gogs server，有空再來放github
3. 用``ARTICLE_URL``和``PAGE_URL``設定好了有日期的網址。[官方教學](http://docs.getpelican.com/en/3.6.3/settings.html#url-settings)用``*_SAVE_AS``來達到沒有``.html``的效果。我個人是覺得直接用``xxx.html``就好啦。

幾個小抱怨點：

1. 預設提供的Makefile不會先clean再輸出，沒注意的話改網址的時候會留下沒用的檔案
2. ``ARTICLE_URL``和``ARTICAL_URL_SAVE_AS``必須兩個同時設定，否則文章會連不到，不太直覺。
