Title: 解決macOS分享的Wi-Fi使Line Points無法使用的問題
Date: 2018-07-07 01:18
Authors: Chih-Hsuan Yen
Slug: mac-wifi-line

Line Points的頁面用Linux hostapd分享出來的Wi-Fi可以正常運作，然而用macOS的網路分享功能無法瀏覽。這個問題困擾了我好久。之前要開Line Points都是先切換成4G，等到要下載App或看影片再換回Wi-Fi。

某天在PTT上看到有人[Wi-Fi環境下用Line無法上傳圖片](https://www.ptt.cc/bbs/Android/M.1529553241.A.5C3.html)。鄉民建議改[MTU](https://zh.wikipedia.org/wiki/%E6%9C%80%E5%A4%A7%E4%BC%A0%E8%BE%93%E5%8D%95%E5%85%83)。我參考[這篇](http://awei791129.pixnet.net/blog/post/58092525-%5B%E7%B6%B2%E8%B7%AF%E5%84%AA%E5%8C%96%5D-%E5%A6%82%E4%BD%95%E6%9C%80%E4%BD%B3%E5%8C%96-mtu-%E5%A4%A7%E5%B0%8F-for-mac-os-x-%E5%92%8C-w)找出最佳MTU值，套用上去之後，macOS分享的Wi-Fi也可以正常使用Line Points了。解決了一個延宕多時的問題，何其暢快！

PS. 何其XX是港劇[公主嫁到](https://zh.wikipedia.org/wiki/%E5%85%AC%E4%B8%BB%E5%AB%81%E5%88%B0)中男主角金多祿的口頭禪之一。
