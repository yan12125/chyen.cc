Title: Migrate Android data from an F-Droid app to a Google Play one
Date: 2021-03-15
Authors: Chih-Hsuan Yen
Slug: migrate-android-app-data

# Motivatation

Recently, login for TTRSS-Reader is [broken after a server update](https://github.com/nilsbraden/ttrss-reader-fork/issues/439).
A working version is uploaded to Google Play, but it presumably takes a long time for the F-Droid version to be updated. I investigated how to migrate data between Android apps as I cannot live without RSS feeds for one day :D

# Migration

1. Clear cache and force stop the application

1. Backup data

        ::shell-session
        htc_ocndugl:/data/data/org.ttrssreader # tar zcvf /data/local/tmp/org.ttrssreader.tar.gz *

1. Uninstall the current version (from F-Droid) and install the other version (from Google Play)

1. Find out the new UID and SELinux context

        ::shell-session
        htc_ocndugl:/data/data/org.ttrssreader # ls -lZ
        total 28
        drwxrws--x   2 u0_a270 u0_a270_cache u:object_r:app_data_file:s0:c14,c257,c512,c768  4096 2021-03-15 12:28 cache
        drwxrws--x   2 u0_a270 u0_a270_cache u:object_r:app_data_file:s0:c14,c257,c512,c768  4096 2021-03-15 12:28 code_cache
   The new uid is `u0_a270` (10270) and the new SELinux label is `u:object_r:app_data_file:s0:c14,c257,c512,c768`.

1. Restore data

        ::shell-session
        htc_ocndugl:/data/data/org.ttrssreader # tar --exclude=cache --exclude=code_cache -zxvf /data/local/tmp/org.ttrssreader.tar.gz

1. Change UID and SELinux context

        ::shell-session
        htc_ocndugl:/data/data/org.ttrssreader # chown 10270 .
        htc_ocndugl:/data/data/org.ttrssreader # chgrp 10270 .
        htc_ocndugl:/data/data/org.ttrssreader # chown -R 10270 app_textures/ app_webview/ databases/ files/ shared_prefs/
        htc_ocndugl:/data/data/org.ttrssreader # chgrp -R 10270 app_textures/ app_webview/ databases/ files/ shared_prefs/
        htc_ocndugl:/data/data/org.ttrssreader # chcon -R -h u:object_r:app_data_file:s0:c14,c257,c512,c768 app_textures/ app_webview/ databases/ files/ shared_prefs/

Here, the `c14,c257,c512,c768` part in a SELinux label is new to me. Reading SELinux documents and tutorials, they seem "categories" for multi-category security (MCS). Few materials discuss how categories are assigned in Android. Here are some references:

* [SELinux concepts](https://source.android.com/docs/security/features/selinux/concepts) mentions that categories are used to "Isolate the app data from access by another app" and "Isolate the app data from one physical user to another." I guess categories plays a similar role to UIDs like `u0_a270` in the DAC world.
* [Security Enhancements for Android](https://github.com/SELinuxProject/selinux-notebook/blob/main/src/seandroid.md#entry-definitions) mentions a `levelFrom=` parameter.
* Furthermore, [SELinux policies for fine-grained protection of Android apps](https://www.politesi.polimi.it/bitstream/10589/147391/3/2019_04_Rossi.pdf) mentions the relation between `levelFrom=` and category numbers. Those numbers do not match what I got from `ls -Z` exactly but close. Probably actual category numbers are related on them.

SELinux adoption in Android makes things complicated as well as interesting!
