Title: Use ECDSA certificate for identification on IRC networks
Date: 2020-10-17 15:56
Authors: Chih-Hsuan Yen
Category: Security
Slug: irc-ecdsa-cert

## Motivation

Recently, I noticed that I can no longer make myself as an operator on the OFTC channel #lxqt.
It turns out that my current SSL client certificate has expired a few days ago.
As it seems that there is no way to [extend expiry of an X509 certificate](https://security.stackexchange.com/a/91513/163400) like what we can do for [PGP keys]({filename}/pages/pgp-handling.md), I need to regenerate a client certificate.

Meanwhile, I heard that ECDSA certificates are [much smaller than RSA certificates](https://letsencrypt.org/2020/09/17/new-root-and-intermediates.html), resulting in faster TLS handshakes.
Although IRC uses persistent connections, so the performance improvement during handshakes does not matter much, it sounds really cool.
So here is my journey - switching to ECDSA certificates on IRC networks.

## Steps

The first step is generating a pair of ECDSA key and certificate. I use [this one line command](https://security.stackexchange.com/a/190646/163400)

    ::shell
    $ openssl req -nodes -newkey ec -pkeyopt ec_paramgen_curve:secp384r1 -keyout irc_client_key.pem -x509 -days 3650 -out irc_client_cert.pem

Following steps for ECDSA certificates are the same as those for RSA certificates. I follow [instructions from OFTC](https://www.oftc.net/NickServ/CertFP/) to create a combined key/certificate file and get its SHA1 fingerprint.

    ::shell
    $ cat irc_client_key.pem >> irc_client_cert.pem
    $ chmod 400 irc_client_cert.pem
    $ openssl x509 -noout -fingerprint -text < irc_client_cert.pem

Steps for libera.chat are similar, except that [a SHA512 fingerprint is needed](https://libera.chat/guides/certfp).

    ::shell
    $ openssl x509 -noout -fingerprint -sha512 < irc_client_cert.pem

Later I need to tell IRC servers about my new fingerprint. As the certificate has expired, I need to identify myself with a password first.

    ::text
    /msg NickServ identify not_my_real_password

And then actually add the new fingerprint.

    ::text
    /msg NickServ cert add new_fingerprint_without_colons

After re-connecting to the IRC server to make sure the new certificate really works, the old fingerprint can be removed.

    ::text
    /msg NickServ cert del old_fingerprint_without_colons

Now I can make myself as an operator 🎉

    ::text
    /msg ChanServ op #lxqt yan12125

After doing whatever channel management work, remove myself as an operator as usual.

    ::text
    /msg ChanServ deop #lxqt yan12125
