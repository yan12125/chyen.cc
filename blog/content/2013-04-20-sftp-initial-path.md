Title: SFTP initial path
Date: 2013-04-20 02:33
Authors: Chih-Hsuan Yen
Slug: sftp-initial-path
Tags: from-wordpress

Google "sftp default path"，大多數是用chroot的方式。但我的需求是一開始先切換到某個home以外的目錄，而不是將user鎖在某個目錄底下。轉念一想，sftp-server也只是一個用user的身份執行的process，其實只要從.bashrc或.profile之類的檔案下手即可。

首先看看Ubuntu的default .bashrc的前幾行(在/etc/skel/.bashrc)：

    :::bash
    # ~/.bashrc: executed by bash(1) for non-login shells.
    # see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
    # for examples

    # If not running interactively, don't do anything
    [ -z "$PS1" ] && return

PS1類似windows下的prompt，當用ssh tunnel或sftp登入時，這個environment variable不存在。如果想要在這幾種登入方法下執行某些指令，必須加在這幾行的前面。我先用"env > ~/tmp_env" 觀察用不同方式登入時environment variables有何不同。結果是用SSH登入時有數個``SSH_``開始的environment variables。如此即可達成需求（加在``.bashrc``的最前面）：

    :::bash
    # if not in an interactive shell, and login by ssh, then it's sftp
    [ -z "$PS1" -a "$SSH_CLIENT" != "" ] && cd ~/export && return

其中``cd ~/export``是想要的initial path，也可以執行其他command，不過要注意的是不能有輸出（echo之類），否則ssh可能會 [無法用sftp登入](http://www.linuxquestions.org/questions/linux-networking-3/ssh-login-works-but-sftp-doesnt-369380/)。
