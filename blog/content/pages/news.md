Title: 新聞轉貼站
Authors: Chih-Hsuan Yen
Slug: news

平常看了不少新聞，有些寫的不錯的新聞看完沒多久就從腦中的記憶淡去，船過水無痕的一般，覺得有點可惜。這邊就放一些連結，當作是生活中另一個面向的紀錄吧！

* 2014/12/23: [平庸的人搶不到舞台！世界變這麼快，都怪3個M](https://www.cw.com.tw/article/5063230)

除了 Market, Mother Nature 和 Moore's Law, Machine Learning 應該也影響滿大的

* 2015/08/09：[令完成已經完成令計畫的計畫](https://www.rfi.fr/tw/中國/20150809-令完成已經完成令計畫的計畫)

神標題XD

* 2017/10/23：[如果時光倒流，我想這麼過大學生活](https://cn.nytimes.com/opinion/20171018/college-advice-professor/zh-hant/)

美國人對大學的想法，和台灣人有滿多不一樣的，挺有趣XD

* 2017/10/27：[世越號搜救紀實》半數以上潛水員從此全身麻痺、便溺失禁，不為人知的最慘「職災」](https://www.storm.mg/lifestyle/346821)

大型災難的救災一向都是爭議很多的問題，嘆

* 2017/11/03：不能一天沒有 PTT，鄉民發動「一人一機器救救 PTT」

[不能一天沒有 PTT，鄉民發動「一人一機器救救 PTT」](http://ccc.technews.tw/2017/11/02/saving-ptt/) (3C新報)

[PTT大當機 鄉民捐10萬送雞排慰勞站方](http://www.cna.com.tw/news/ait/201711020300-1.aspx) (中央社)

[林志銘](https://www.facebook.com/permalink.php?story_fbid=10155802087316310&id=633286309) (Facebook)

PTT這次壞好久...@@

然後3C新報的標題很鄉民XD

* 2017/11/03：[PTT 斷線 5 天的影響？看看這些資深鄉民怎麼說](http://technews.tw/2017/11/03/senior-ptter-says-things-about-bbs-ptt-offline/)

寫的還不錯的討論

* 2021/10/13: [微軟示範在無TPM、VBS硬體的電腦上，駭進自家Windows 11系統有多容易](https://www.techbang.com/posts/90645-microsoft-windows-system-vbs-protected-computers)

最後一段滿幽默的XD

> 所以，看到這裡，你的電腦如果沒有TPM2.0的話，微軟一天告訴你不要裝、一天告訴你可以繞過、一天又告訴你很危險，感覺微軟好像有兩個大腦在左右較勁。你覺得你到底是要裝Win11或是繼續Win10再戰十年？
